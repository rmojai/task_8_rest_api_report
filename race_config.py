class Config(object):
    SECRET_KEY = 'dev'
    DEBUG = True
    PATH = r'..\instance\logs'


class Testing(Config):
    TESTING = True
