from dict2xml import dict2xml
from flask import Response, Blueprint, jsonify, request, current_app
from flask_restful import Resource

from src.report.report import build_report, sort_report

api_race = Blueprint('api_race', __name__, url_prefix='/api/v1')


class Report(Resource):
    def get(self):
        order = request.args.get('order')
        format = request.args.get('format')
        if order == 'desc':
            order = bool(order)
        else:
            order = bool()
        winlist = sort_report(build_report(current_app.config['PATH']), reverse=order)

        report = {}
        for drivers, laps in winlist.items():

            driver_name = winlist[drivers]['name']
            driver_team = winlist[drivers]['team']
            driver_time = winlist[drivers]['time']

            num = 1
            for i in report:
                num += 1

            report[num] = {
                'name': driver_name,
                'team': driver_team,
                'time': driver_time,
            }

        if format == 'json' or format == None:
            return jsonify(report)
        elif format == 'xml':
            report_xml = dict2xml(report)
            return Response(report_xml, mimetype='application/xml')
