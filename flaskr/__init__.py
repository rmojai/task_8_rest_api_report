import os

from flasgger import Swagger
from flask import Flask

from api import api_race
from flaskr.routes import race


def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.register_blueprint(race)
    app.register_blueprint(api_race)
    app.config.from_object('race_config.Config')
    Swagger(app, template_file='../api/report.yml')
    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_object('race_config.Testing')

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    return app
