from flask import Blueprint, render_template, request
import os
import race_config
from src.report.report import read_abbr, build_report, sort_report

race = Blueprint('race', __name__, template_folder='templates')


@race.route('/', methods=['POST', 'GET'])
@race.route('/report/', methods=['POST', 'GET'])
def common_statistic():
    order = request.args.get('order')
    if order == 'desc':
        order = bool(order)
    else:
        order = bool()
    winlist = sort_report(build_report(race_config.Config.PATH), reverse=order)
    return render_template('common_statistic.html', winlist=winlist)


@race.route('/report/drivers/', methods=['POST', 'GET'])
def drivers_list():
    order = request.args.get('order')
    driver_id = request.args.get('driver_id')
    abbrs = read_abbr(os.path.join(race_config.Config.PATH, 'abbreviations.txt'))
    if driver_id is not None:
        info = build_report(race_config.Config.PATH)
        driver_request = render_template('driver_info.html', info=info, driver=driver_id, abbrs=abbrs)
    elif order == 'desc':
        order = bool(order)
        abbrs = dict(sorted(abbrs.items(), key=lambda item: item[1]['name'], reverse=order))
        driver_request = render_template('drivers_list.html', abbrs=abbrs)
    else:
        order = bool()
        abbrs = dict(sorted(abbrs.items(), key=lambda item: item[1]['name'], reverse=order))
        driver_request = render_template('drivers_list.html', abbrs=abbrs)

    return driver_request
